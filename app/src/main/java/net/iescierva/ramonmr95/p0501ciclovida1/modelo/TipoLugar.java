package net.iescierva.ramonmr95.p0501ciclovida1.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import net.iescierva.ramonmr95.p0501ciclovida1.R;

public enum TipoLugar implements Parcelable {
    OTROS ("Otros", R.drawable.otros),
    RESTAURANTE ("Restaurante", R.drawable.restaurante),
    BAR ("Bar", R.drawable.bar),
    COPAS ("Copas", R.drawable.copas),
    ESPECTACULO ("Espectáculo", R.drawable.espectaculos),
    HOTEL ("Hotel", R.drawable.hotel),
    COMPRAS ("Compras", R.drawable.compras),
    EDUCACION ("Educación", R.drawable.educacion),
    DEPORTE ("Deporte", R.drawable.deporte),
    NATURALEZA ("Naturaleza", R.drawable.naturaleza),
    GASOLINERA ("Gasolinera", R.drawable.gasolinera);

    private final String texto;
    private final int recurso;

    TipoLugar(String texto, int recurso) {
        this.texto = texto;
        this.recurso = recurso;
    }

    public String getTexto() { return texto; }
    public int getRecurso() { return recurso; }

    public static String[] getNombres() {
        String[] resultado = new String[TipoLugar.values().length];
        for (TipoLugar tipo : TipoLugar.values()) {
            resultado[tipo.ordinal()] = tipo.texto;
        }
        return resultado;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ordinal());;
    }


    public static final Creator<TipoLugar> CREATOR = new Creator<TipoLugar>() {
        @Override
        public TipoLugar createFromParcel(Parcel in) {
            return values()[in.readInt()];
        }

        @Override
        public TipoLugar[] newArray(int size) {
            return new TipoLugar[size];
        }
    };
}

