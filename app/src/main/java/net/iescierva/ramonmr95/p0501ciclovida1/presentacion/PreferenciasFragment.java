package net.iescierva.ramonmr95.p0501ciclovida1.presentacion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.iescierva.ramonmr95.p0501ciclovida1.R;

public class PreferenciasFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}