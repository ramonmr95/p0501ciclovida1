package net.iescierva.ramonmr95.p0501ciclovida1.presentacion;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.ramonmr95.p0501ciclovida1.Aplicacion;
import net.iescierva.ramonmr95.p0501ciclovida1.R;
import net.iescierva.ramonmr95.p0501ciclovida1.casos_uso.CasosUsoLugar;
import net.iescierva.ramonmr95.p0501ciclovida1.datos.Lugares;
import net.iescierva.ramonmr95.p0501ciclovida1.modelo.Lugar;
import net.iescierva.ramonmr95.p0501ciclovida1.modelo.TipoLugar;


public class EdicionLugarActivity extends AppCompatActivity {

    private Lugares lugares;
    private CasosUsoLugar usoLugar;
    private int pos;
    private Lugar lugar;
    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText comentario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edicion_lugar);
        Bundle extras = getIntent().getExtras();
        pos = extras.getInt("id", 0);
        lugar = extras.getParcelable("lugar");
        lugares = ((Aplicacion) getApplication()).lugares;
        usoLugar = new CasosUsoLugar(this, lugares);
        //lugar = lugares.elemento(pos);
        addSpinner();
        actualizaVistas();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edicion_lugar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cancelEditButton:
                finish();
            case R.id.AcceptEditButton:
                guardarEdit();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void addSpinner() {
        tipo = findViewById(R.id.tipo);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());
        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        tipo.setAdapter(adaptador);
        tipo.setSelection(lugar.getTipo().ordinal());
    }

    public void actualizaVistas() {
        nombre = findViewById(R.id.NombreEditText);
        nombre.setText(lugar.getNombre());

        direccion = findViewById(R.id.direccionEditText);
        direccion.setText(lugar.getDireccion());

        telefono = findViewById(R.id.telefonoEditText);
        telefono.setText(String.valueOf(lugar.getTelefono()));

        url = findViewById(R.id.urlEditText);
        url.setText(lugar.getUrl());

        comentario = findViewById(R.id.comentariosEditText);
        comentario.setText(lugar.getComentario());
    }

    public void guardarEdit() {
        lugar.setNombre(nombre.getText().toString());
        lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
        lugar.setDireccion(direccion.getText().toString());
        lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
        lugar.setUrl(url.getText().toString());
        lugar.setComentario(comentario.getText().toString());
        usoLugar.guardar(pos, lugar);
        finish();
    }
}
