package net.iescierva.ramonmr95.p0501ciclovida1;

import android.app.Application;

import net.iescierva.ramonmr95.p0501ciclovida1.datos.Lugares;
import net.iescierva.ramonmr95.p0501ciclovida1.datos.LugaresLista;
import net.iescierva.ramonmr95.p0501ciclovida1.presentacion.AdaptadorLugares;

public class Aplicacion extends Application {

    public Lugares lugares;
    public AdaptadorLugares adaptador;

    public Aplicacion() {
        lugares = new LugaresLista();
        adaptador = new AdaptadorLugares(lugares);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Lugares getLugares() {
        return lugares;
    }
}