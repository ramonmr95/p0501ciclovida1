package net.iescierva.ramonmr95.p0501ciclovida1.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import net.iescierva.ramonmr95.p0501ciclovida1.datos.GeoException;


public class Lugar implements Parcelable {
    private String nombre;
    private String direccion;
    private GeoPunto posicion;
    private String foto;
    private int telefono;
    private String url;
    private String comentario;
    private long fecha;
    private float valoracion;

    private TipoLugar tipo;

    public Lugar(String nombre, String direccion, double latitud,
                 double longitud, TipoLugar tipo, int telefono, String url, String comentario,
                 int valoracion) throws GeoException {
        fecha = System.currentTimeMillis();
        posicion = new GeoPunto(latitud, longitud);
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.url = url;
        this.comentario = comentario;
        this.valoracion = valoracion;
        this.tipo = tipo;
    }

    public Lugar() {
        fecha = System.currentTimeMillis();
        posicion =  new GeoPunto();
        tipo = TipoLugar.OTROS;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Lugar{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", posicion=" + posicion +
                ", foto='" + foto + '\'' +
                ", telefono=" + telefono +
                ", url='" + url + '\'' +
                ", comentario='" + comentario + '\'' +
                ", fecha=" + fecha +
                ", valoracion=" + valoracion +
                ", tipo=" + tipo +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nombre);
        dest.writeString(direccion);
        dest.writeParcelable(posicion, 0);
        dest.writeSerializable(tipo);
        dest.writeInt(telefono);
        dest.writeString(url);
        dest.writeString(comentario);
        dest.writeLong(fecha);
        dest.writeFloat(valoracion);
    }

    public Lugar(Parcel in) {
        this.nombre = in.readString();
        this.direccion = in.readString();
        this.posicion = in.readParcelable(GeoPunto.class.getClassLoader());
        this.tipo = (TipoLugar)in.readSerializable();
        this.telefono = in.readInt();
        this.url = in.readString();
        this.comentario = in.readString();
        this.fecha = in.readLong();
        this.valoracion = in.readFloat();
    }

    public static final Creator<Lugar> CREATOR = new Creator<Lugar>() {
        @Override
        public Lugar createFromParcel(Parcel in) {
            return new Lugar(in);
        }

        @Override
        public Lugar[] newArray(int size) {
            return new Lugar[size];
        }
    };
}


