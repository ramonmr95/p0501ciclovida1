package net.iescierva.ramonmr95.p0501ciclovida1.presentacion;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


public class PreferenciasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                .commit();
    }
}