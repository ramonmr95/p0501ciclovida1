package net.iescierva.ramonmr95.p0501ciclovida1.casos_uso;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import net.iescierva.ramonmr95.p0501ciclovida1.presentacion.AcercaDeActivity;
import net.iescierva.ramonmr95.p0501ciclovida1.presentacion.PreferenciasActivity;
import net.iescierva.ramonmr95.p0501ciclovida1.presentacion.VistaLugarActivity;

public class CasosUsoActividades {

    private Activity actividad;

    public CasosUsoActividades(Activity actividad) {
        this.actividad = actividad;
    }

    public void lanzarAcercaDe(View view){
        Intent i = new Intent(actividad, AcercaDeActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarPreferencias(View view){
        Intent i = new Intent(actividad, PreferenciasActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarVistaLugarActividad(View view) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        actividad.startActivity(i);
    }

}
