package net.iescierva.ramonmr95.p0501ciclovida1.datos;

public class GeoException extends Exception {

    public GeoException(String s) {
        super(s);
    }

}
